\chapter{Introduction}

\section{CubeSats}

CubeSats are tiny satellites with standardized cubic shape (as multiples of 10x10x10 cm\textsuperscript{3}), which are stored inside a standardized container during launch and deployed once the orbit is reached by the rocket upper stage. CubeSats are typically launched in bulks as secondary payloads and thus provide more affordable launch prices. Their standardized form factor makes it possible to be launched into orbit from a variety of rockets, and even from the International Space Station (ISS).

In 1999 the CubeSat standard was created as a joint effort between Professor Jordi Puig-Suari and Professor Bob Twiggs from California Polytechnic State University and Stanford University, respectively \cite{heidt2000cubesat}. The standard specifies mainly the mechanical interface requirements of a 1.33 kg, 10x10x10 cm\textsuperscript{3} nanosatellite and multiples thereof. Satellites adhering to this standard would be compatible with the Poly-PicoSatellite Orbital Deployer (P-POD), a standardized launch container developed at Stanford University \cite{nason2002development}. The P-POD is attached to the upper stage of a launch rocket, carries between one and three of such CubeSats and deploys them into orbit. As such, the P-POD provides a first degree decoupling of the interface between satellite and launch rocket and eases the launcher integration process significantly.

The motivation behind the CubeSat standard was to enable graduate students to design, build, test and operate satellites within their academic curriculum. The first CubeSats were launched in June 2003. The number of CubeSats launched has rapidly increased in recent years. The success of CubeSats is due to its standardized interface with respect to the launcher integration. This has led to cheaper launch costs and an accelerated launch preparation schedule.

Triggered by the success of CubeSats, a handful of start-up companies appeared during the second half of the first decade of 2000, and more followed. Founded mainly by graduates who worked on CubeSat missions during their studies, these companies kept strong ties with their host university, and focused mainly on supporting research and educational missions. Yet, despite having this academic background, the products were sold closed source, with some companies offering the design information for a significant surplus charge. Around the same time, industry and military entered the fast growing CubeSat sector and launched missions \cite{taraba2009boeing}. By 2013 a dramatic increase of CubeSats deployed in space is noticeable, as shown in Figure \ref{fig:CubeSats_launched_by_year}. Most of the recent CubeSats were launched as clusters or fleets under the flag of private companies. Nonetheless, the academic world takes the largest share among CubeSat developers, as shown in Figure \ref{fig:CubeSats_launched_by_ownership}\footnote{https://sites.google.com/a/slu.edu/swartwout/home/cubesat-database}. 

\begin{figure}[h]
\centering\includegraphics[width=0.8\linewidth]{fig/cubesats_launched_from_2000_to_2020}
\caption{CubeSats launched from 2000 to 2020 (copyright M. Swartwout)}
\label{fig:CubeSats_launched_by_year}
\end{figure}

\begin{figure}[h]
\centering\includegraphics[width=0.8\linewidth]{fig/cubesats_launched_from_2003_to_2020_by_ownership}
\caption{CubeSats launched from 2000 to 2020 by ownership (copyright M. Swartwout)}
\label{fig:CubeSats_launched_by_ownership}
\end{figure}

University-built CubeSats will continue to have a strong impact on the future of the CubeSat program. And while CubeSats are an ideal tool for teaching hands-on space technology, they also offer a great chance for opening up the access to space technology to a much larger audience, including students and individuals from developing countries \cite{scholz2015toward}. 

\section{Space Standards}

Unfortunately it is still a common practice to design each CubeSat specifically for its mission objectives without much consideration of re-usability. Thus, the reuse of such systems is usually very limited. While some developers may have established in-house standards for their own CubeSat projects, those are in most cases not known to nor adapted by other missions.

In fact, there exists little interface standardization for CubeSats which can be used by equipment and instrument developers. While it is true that there are a limited number of physical interfaces applicable for practical use in the space environment, the services and access to these interfaces vary considerably between implementations.

Within the CubeSat developer community there have so far been very few significant attempts to further standardize CubeSat missions. Apart from the CubeSat design specification \cite{cubesat_design_specification}, there exist only a few de-facto standards or best practices. Typically, the solutions that have been developed for CubeSat missions by individual developers are tied very much to the peculiarities of their CubeSat payload and mission objective, with little effort to create sustainable and reusable system components.

The result is that a multitude of CubeSat design solutions are in place, with each mission either inheriting past solutions or developing new ones. However, an increase in the number and complexity of missions and the cost of developing state-of-the-art high-speed data interfaces should trigger a re-thinking within the CubeSat community, towards application of a more standardized approach.

Standardization is an important tool to reduce risks, cost and improve both quality and communication between parties during the preparation and execution of space missions. This is true for any spacecraft mission, and CubeSats are no exception to it.

\subsubsection{Organizations}

A number of organizations exist that are concerned with the development of international standards applicable for space projects. As those standards are often based on vast experience from experts in the field, including lessons learned from past missions, they provide a valuable for meaningful standardization of almost any aspect of typical space missions. For most of these standards the particularities of the spacecraft are not important and hence can be applied to traditional satellites and CubeSats alike.

This book is concerned with the mapping of available international space standards to the various aspects of developing and running a CubeSat mission. The selection criteria for standards organizations were based on the following:

\begin{itemize}
\item Open: Standards shall be openly available to anyone, preferable through the internet.
\item Free: Standards shall be free of charge and fees.
\item International: Standards shall be available in English and address not national interests but an international audience.
\item Up-to-date: Standards shall be implementable with state-of-the-art components.
\end{itemize}

The organizations found to comply well with these criteria are introduced briefly in the following sections.

The \textbf{Consultative Committee for Space Data Systems (CCSDS)}\footnote{ccsds.org} was founded in 1982 for governmental and quasi-governmental space agencies to discuss and develop standards for space data and information systems. Currently composed of "eleven member agencies, twenty-eight observer agencies, and over 140 industrial associates," the CCSDS works to support collaboration and interoperability between member agencies through the establishment of data and system standards. The activities of the CCSDS are organized around six topic areas (see Figure \ref{fig:CCSDS Topic Areas}) and composed of many working groups.

The \textbf{European Cooperation for Space Standardization (ECSS)}\footnote{ecss.nl} was initiated to harmonize the requirements from existing standards for space projects, and to provide a single, coherent set of standards for use in (but not limited to) all European space systems development and operation.

The goal of ECSS is to develop a common set of consistent standards for hardware, software, information and activities to be applied in space projects, so that life cycle cost are minimized, while continually improving the quality, functional integrity, reliability and compatibility of all elements of the project. It covers the disciplines shown in Figure \ref{fig:ECSS Disciplines}. 

\begin{figure}[h]
\centering\includegraphics[scale=0.6]{fig/ccsds_topic_areas}
\caption{CCSDS Topic Areas}
\label{fig:CCSDS Topic Areas}
\end{figure}

\begin{figure}[h]
\centering\includegraphics[width=1.0\linewidth]{fig/ecss_disciplines}
\caption{High-level representation of ECSS disciplines tree}
\label{fig:ECSS Disciplines}
\end{figure}

\section{How To Use This Book}

The purpose of this text is to help improve the overall reliability of CubeSats (in particular from academia and research) and to increase the level of cooperation among CubeSat developers. The application of standards and the acceptance to open source software and hardware are the two primary pillars upon which these goals can be achieved.

\textbf{This book however does not prescribe in any way what CubeSat developers have to do or which standards shall be applicable for their project}. The book is only a review of existing space standards (from ECSS and CCSDS) and their potential application within CubeSat missions. It is be up to the CubeSat developer team (or the top level customer) to decide which of those standards are selected as binding.

The recommended approach for CubeSat developers is to first decide upon which of those standards they want to use, and then in the second step tailor or customize those standards to their needs. 

Further, it is recommended to produce all the documents listed as deliverables in the various sections. Although this may appear as an overwhelming task at first that requires a lot of paperwork, it lays in fact the foundation of a project. It is up to the project team to decide how much effort they put into each of these documents, but it will give a guiding structure to all project related activities and for all the people involved. Last but not least, following this document layout will help in understanding and navigating through other CubeSat projects much quicker.

The second part of the book then presents a large number of standards that are related to system technology and communications protocols. They are categorized into domains of potential application. For example, for onboard communication, the ECSS-CAN bus standard is presented, as it is a standardized space communication bus that is suitable and feasible for implementation in CubeSats. Note however that this part of the book is \textbf{not} a review of technology used on CubeSats or a recipe on how to build a CubeSat. It is rather to show what standards are available that can be possibly used in a CubeSat mission. Most of those standards are concerned with data exchange protocols and interface standardization.